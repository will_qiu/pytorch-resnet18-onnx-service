# Pytorch-resnet18-onnx-service

## Getting Started

## Windows
### Pre-requirements

**nvidia-driver**
- [Tutorial-nvidia-driver](https://www.nvidia.com/Download/index.aspx?lang=en-us)

**WSL**
- [Tutorial-WSL](https://www.how2shout.com/how-to/how-to-install-ubuntu-20-04-wl2-on-windows-11.html)

**DOCKER DESKTOP**
- [Tutorial-docker-desktop](https://www.docker.com/products/docker-desktop/)

**NGINX**

Because WLS and the local Windows service have different IP addresses, you need to install nginx to proxy the IP.

- [Tutorial-Nginx](http://nginx.org/en/download.html)

## Ubuntu
### Pre-requirements

Install **nvidia-driver**, **nvidia-docker** and **docker** before installing the docker container.

- [Tutorial-nvidia-driver](https://docs.nvidia.com/datacenter/tesla/tesla-installation-notes/index.html)

- [Tutorial-docker](https://docs.docker.com/engine/install/ubuntu/)

- [Tutorial-nvidia-docker](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#docker)

### Building image
```shell
sudo chmod u+x ./docker/*.sh
sudo ./docker/build.sh
```

### Run docker container
```shell
sudo chmod u+x ./docker/*.sh
sudo ./docker/run.sh
```

### Run webAPI service

```shell
gunicorn -b 0.0.0.0:5000 --workers 1 --threads 100 webapi:app
```

### Testing

```python
python3 client/client_ws_100pics.py -p <folder/files> --url 0.0.0.0:5000
python3 client/client_ws_batch.py -p <folder/files> --batch 8 --url 0.0.0.0:5000
python3 client/client_ws_batch_pickle.py -p <folder/files> --batch 8 --url 0.0.0.0:5000
```
- p: The path of inference images. you can input the folder path or the image path.
- batch: Input batch size.
- url: Service IP and port- ex: 127.0.0.1:3000.
- nginx: Confirm if there is an Nginx reverse proxy.

<details>
    <summary> Result
    </summary>
    <div align="center">
        <img width="90%" height="90%" src="./docs/ws_100_edit.png">
        <img width="90%" height="90%" src="./docs/batch_edit.png">
        <img width="90%" height="90%" src="./docs/batch_pickle_edit.png">
    </div>
</details>